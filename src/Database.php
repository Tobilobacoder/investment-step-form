<?php

class Database {
    private $servername;
    private $username;
    private $password;
    private $dbname;
    private $charset;

    public function connect(){
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "";
        $this->dbname ="investmentp";
        $this->charset ="utf8mb4";

try{
    $dsn = "mysql:host=".$this->servername.";dbname=".$this->dbname.";charset=".$this->charset;
    $conn = new PDO($dsn,$this->username,$this->password);
    return $conn;
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOExeption $e){
        echo "Connection failed : ".$e->getMessage();
    }

 }
}


?>
