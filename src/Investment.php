<?php
/**
 * Investment application form class
 * Method to select all Banks
* Method to select account services
* Method to select investor type
* Method to insert applicants
 */
class Investment {


  // Methood to get all Banks
  public function getAllBanks(){
    $db = new Database;
    $conn = $db->connect();
    try {
      $sql = "SELECT * FROM banks";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $array = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($array != ''){
                // var_dump($array);
                return $array;
            }
    } catch (PDOExeption $e) {
       echo $e->getMessage();
    }
  }


  // method to select account services
  public function getAccountServices(){
    $db = new Database;
    $conn = $db->connect();
    try {
      $sql = "SELECT * FROM account_services";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $array = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($array != ''){
                // var_dump($array);
                return $array;
            }
    } catch (PDOExeption $e) {
       echo $e->getMessage();
    }

  }

  //method to get investors type
  public function getInvestorType(){
    $db = new Database;
    $conn = $db->connect();
    try {
      $sql = "SELECT * FROM investor_types";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $array = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($array != ''){
                // var_dump($array);
                return $array;
            }
    } catch (PDOExeption $e) {
       echo $e->getMessage();
    }

  }

  //Method insert investors applicants
  public function insertApplicant($surname, $name, $othernames, $residential_address, $phone_number, 	$bvn,
    $gender, $date_of_birth, 	$occupation, $employer, $employment_position, $office_address, $nationality,
  $bank_account_number, $bank_account_name, $bank_id, $next_of_kin_name, $next_of_kin_address, $next_of_kin_phone_number,
  $next_of_kin_relationship, $next_of_kin_email, $account_service_id, $investment_amount, $tenor, $email, $investor_type_id){
    $db = new Database;
    $conn = $db->connect();

    try {
      $sql = "INSERT INTO applicants(surname, name, othernames, residential_address, phone_number, 	bvn,
      	gender, date_of_birth, 	occupation, employer, employment_position, office_address, nationality,
      bank_account_number, bank_account_name, bank_id, next_of_kin_name, next_of_kin_address, next_of_kin_phone_number,
      next_of_kin_relationship, next_of_kin_email, account_service_id, investment_amount, tenor, email, investor_type_id)

      VALUE(:surname, :name, :othernames, :residential_address, :phone_number, :bvn,
      	:gender, :date_of_birth, 	:occupation, :employer, :employment_position, :office_address, :nationality,
      :bank_account_number, :bank_account_name, :bank_id, :next_of_kin_name, :next_of_kin_address, :next_of_kin_phone_number,
      :next_of_kin_relationship, :next_of_kin_email, :account_service_id, :investment_amount, :tenor, :email, :investor_type_id)";
      $stmt = $conn->prepare($sql);

      $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
      $stmt->bindParam(':name', $name, PDO::PARAM_STR);
      $stmt->bindParam(':othernames', $othernames, PDO::PARAM_STR);
      $stmt->bindParam(':residential_address', $residential_address, PDO::PARAM_STR);
      $stmt->bindParam(':phone_number', $phone_number, PDO::PARAM_STR);
      $stmt->bindParam(':bvn', $bvn, PDO::PARAM_STR);
      $stmt->bindParam(':gender', $gender, PDO::PARAM_STR);
      $stmt->bindParam(':date_of_birth', $date_of_birth, PDO::PARAM_STR);
      $stmt->bindParam(':occupation', $occupation, PDO::PARAM_STR);
      $stmt->bindParam(':employer', $employer, PDO::PARAM_STR);
      $stmt->bindParam(':employment_position', $employment_position, PDO::PARAM_STR);
      $stmt->bindParam(':office_address', $office_address, PDO::PARAM_STR);
      $stmt->bindParam(':nationality', $nationality, PDO::PARAM_STR);
      $stmt->bindParam(':bank_account_number', $bank_account_number, PDO::PARAM_STR);
      $stmt->bindParam(':bank_account_name', $bank_account_name, PDO::PARAM_STR);
      $stmt->bindParam(':bank_id', $bank_id, PDO::PARAM_STR);
      $stmt->bindParam(':next_of_kin_name', $next_of_kin_name, PDO::PARAM_STR);
      $stmt->bindParam(':next_of_kin_address', $next_of_kin_address, PDO::PARAM_STR);
      $stmt->bindParam(':next_of_kin_phone_number', $next_of_kin_phone_number, PDO::PARAM_STR);
      $stmt->bindParam(':next_of_kin_relationship', $next_of_kin_relationship, PDO::PARAM_STR);
      $stmt->bindParam(':next_of_kin_email', $next_of_kin_email, PDO::PARAM_STR);
      $stmt->bindParam(':account_service_id', $account_service_id, PDO::PARAM_STR);
      $stmt->bindParam(':investment_amount', $investment_amount, PDO::PARAM_STR);
      $stmt->bindParam(':tenor', $tenor, PDO::PARAM_STR);
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $stmt->bindParam(':investor_type_id', $investor_type_id, PDO::PARAM_STR);

      $result = $stmt->execute() or die(print_r($stmt->errorInfo(), true));
            if($result != ''){
                return $result;
            }
    } catch (PDOExeption $e) {
        echo $e->getMessage();
    }
  }

  public function sendMail($surname, $name, $othernames, $email){
    try {
      $EmailTo = "info@corastone.capital";
      $Subject = "New message from Investment page";

       // prepare email body text
       $Body = "";
       $Body .= "Name: ";
       $Body .= $surname;
       $Body .= $name;
       $Body .= $othernames;
       $Body .= "\n";
       $Body .= "Your request have been received";
       $Body .= "\n";

       // send email
       $success = mail($EmailTo, $Subject, $Body, "From:".$email);
        if ($success ){
           return true;
        }else{
                return false;
            }
        } catch (PDOExeption $e) {
            echo $e->getMessage();
        }
      }
      public function checkEmailExist($email){
        $db=new Database ;
       $conn =$db->connect();
       try{
           $sql = "SELECT email FROM applicants WHERE email=:email";
           $stmt = $conn->prepare($sql);
           $stmt->bindParam("email", $email, PDO::PARAM_STR);
           $stmt->execute();
           if($stmt->rowCount() > 0){
               return true;
           }else{
               return false;
           }
       }catch(PDOException $e){
           echo $e->getMessage();
       }
      }
      public function checkBVNExist($bvn){
        $db = new Database;
       $conn =$db->connect();
       try{
           $sql = "SELECT bvn FROM applicants WHERE bvn=:bvn";
           $stmt = $conn->prepare($sql);
           $stmt->bindParam("bvn", $bvn, PDO::PARAM_STR);
           $stmt->execute();
           if($stmt->rowCount() > 0){
               return true;
           }else{
               return false;
           }
       }catch(PDOException $e){
           echo $e->getMessage();
       }
      }
}

 ?>
