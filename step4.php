
<?php
session_start();
// if(!isset($_SESSION['next_of_kin_name'])){
//         header("location: step3.php");
//     }
include  ('vendor/autoload.php');
$investment = new Investment;
// surname, name, othernames, residential_address, phone_number, 	bvn,
// gender, date_of_birth, 	occupation, employer, employment_position, office_address, nationality,
// bank_account_number, bank_account_name, bank_id, next_of_kin_name, next_of_kin_address, next_of_kin_phone_number,
// next_of_kin_relationship, next_of_kin_email, account_service_id, investment_amount, tenor, email, investor_type_id, status, user_id

   if(isset($_POST['applicants'])){
       
        $_SESSION['investor_type_id'] = $_POST['investor_type_id'];
        $_SESSION['investment_amount'] = $_POST['investment_amount'];
        $_SESSION['tenor']= $_POST['tenor'];
        $_SESSION['bvn'] = $_POST['bvn'];
        $_SESSION['bank_id']= $_POST['bank_id'];
        $_SESSION['bank_account_number'] = $_POST['bank_account_number'];
        $_SESSION['bank_account_name'] = $_POST['bank_account_name'];
        $_SESSION['account_service_id'] = $_POST['account_service_id'];

		    if($_SESSION['investor_type_id'] == ""){
             $applicants_error_message= 'Pleased eneter your investor type!';
        }elseif($_SESSION['investment_amount'] ==""){
             $applicants_error_message='Pleased enter your investment amount!';
        }elseif($_SESSION['tenor'] == ""){
             $applicants_error_message= 'Pleased eneter your tenor!';
        }elseif($_SESSION['bvn'] == ""){
          $applicants_error_message= 'Pleased eneter your bvn!';
        }elseif($_SESSION['bank_id']== ""){
          $applicants_error_message= 'Pleased eneter your bank!';
        }elseif($_SESSION['bank_account_number'] == ""){
            $applicants_error_message= 'Pleased eneter your bank account number!';
        }elseif($_SESSION['bank_account_name'] == ""){
            $applicants_error_message= 'Pleased eneter your bank account name!';
            //Check if BVN exist
        }elseif($investment->checkBVNExist($_SESSION['bvn'])){
              $applicants_error_message= 'Investors with this BVN already exist! Pleased check your BVN.';
        
            // Check if Email Exist
        }elseif($investment->checkEmailExist($_SESSION['email'])){
            $applicants_error_message= 'Email already exist! Pleased go back to step 1 to enter another Email or Re-enter this.';
            //Check if BVN exist
        }else{
          $surname = $_SESSION['surname'];
          $name = $_SESSION['name'];
          $othernames = $_SESSION['othernames'];
          $residential_address = $_SESSION['residential_address'];
          $phone_number = $_SESSION['phone_number'] ;
          $bvn = $_SESSION['bvn'];
          $gender = $_SESSION['gender'];
          $date_of_birth = $_SESSION['date_of_birth'];
          $occupation = $_SESSION['occupation'];
          $employer = $_SESSION['employer'] ;
          $employment_position = $_SESSION['employment_position'];
          $office_address =  $_SESSION['office_address'];
          $nationality = $_SESSION['nationality'];
          $bank_account_number = $_SESSION['bank_account_number'];
          $bank_account_name =   $_SESSION['bank_account_name'];
          $bank_id = $_SESSION['bank_id'];
          $next_of_kin_name = $_SESSION['next_of_kin_name'];
          $next_of_kin_address = $_SESSION['next_of_kin_address'];
          $next_of_kin_phone_number = $_SESSION['next_of_kin_phone_number'] ;
          $next_of_kin_relationship = $_SESSION['next_of_kin_relationship'];
          $next_of_kin_email = $_SESSION['next_of_kin_email'] ;
          $account_service_id = $_SESSION['account_service_id'] ;
          $investment_amount  = $_SESSION['investment_amount'] ;
          $tenor = $_SESSION['tenor'];
          $email = $_SESSION['email'];
          $investor_type_id = $_SESSION['investor_type_id'];


            $result = $investment->insertApplicant($surname, $name, $othernames, $residential_address, $phone_number, 	$bvn,
					    $gender, $date_of_birth, 	$occupation, $employer, $employment_position, $office_address, $nationality,
					  $bank_account_number, $bank_account_name, $bank_id, $next_of_kin_name, $next_of_kin_address, $next_of_kin_phone_number,
					  $next_of_kin_relationship, $next_of_kin_email, $account_service_id, $investment_amount, $tenor, $email, $investor_type_id);
            // $result.= $investment->sendMail($surname, $name, $othernames, $email);
            if($result){
               session_unset();			
            $success = true;
             header("refresh:7; url=step1.php");
             
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Investment Application Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,700,800|Roboto:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- Custorm sytle css -->
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

<nav class="navbar navbar-expand-sm bg-white shadow-sm">
  <!-- Brand/logo -->
  <a class="navbar-brand logo" href="#">
    <img src="https://referral.coralstone.capital/images/logo.png" alt="logo" style="width:100px;">
  </a>
</nav>

    <!-- <div class="containerse">
        <ul class="progressbarse">
            <li >Bio-Data</li>
            <li class="active">Employment Info</li>
            <li>Next of Kin</li>
            <li >Investment Details</li>
        </ul>
    </div>
    <br> -->
    <div class="container py-5">
      
        <div class="row col-12">
          <div class="col-12 col-md-5">
            <h5 class="">Investment Registration</h5>
              
          </div>
          
           <div class="col-12 col-md-7">
                <ul id="progressbar" class="text-center">
                    <li class="active step0">Bio-Data</li>
                    <li class="active step0">Employment Info</li>
                    <li class="active step0">Next of Kin</li>
                    <li class="active step0">Investment Details</li>
                </ul>
            </div>
        </div>
      </div>
     
      <div class="container bg-white rounded">
      <?php
          if(isset($applicants_error_message) and $applicants_error_message != ""){
              echo '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                  <span class="badge badge-pill badge-danger">Danger  <br></span> '
                  .$applicants_error_message.'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>';
          }

					  if(isset($success) and  $success == true){
					    	echo '<script type="text/javascript"> swal("Thank You!", "Your investment application has been submited successfully!", "success"); </script>';
					  }

                      ?>
        <div class="row col-12 py-5">
            <div class="col-12 col-md-4">
            <h4 class="py-2">Investment Details</h4>
            <p>How much do you want to invest on our plantform and where would you want us to pay your investment proceeds to? </p>
              
          </div>
           <div class="col-12 col-md-8 py-5">
               <form action="" method="post">
                      <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Investment Type">Investment Type</label>
                            <select name="investor_type_id" id="Investment Type" class="form-control bg-light">
                             
                                <?php 	$investors = $investment->getInvestorType();
                                    foreach ($investors as $invest) {
                                  ?>
                                <option value="<?php echo $invest['id'] ?>" class="option"><?php echo $invest['name'] ?></option>

                                <?php 	}  ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="Amount">Amount</label>
                          <input type="number" name="investment_amount" value="<?php if (isset($_SESSION['investment_amount'])) { echo $_SESSION['investment_amount']; } ?>" class="form-control bg-light"  placeholder="Amount">
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="Tenure">Tenure</label>
                          <input type="number" value="<?php if (isset($_SESSION['tenor'])) { echo $_SESSION['tenor']; } ?>" name="tenor" class="form-control bg-light"  placeholder="Tenure">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="BVN">BVN</label>
                          <input type="number" value="<?php if (isset($_SESSION['bvn'])) { echo $_SESSION['bvn']; } ?>" name="bvn"  maxlength="11"  class="form-control bg-light"  placeholder="BVN Number">
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="bank">Bank</label>
                          <select name="bank_id" id="Investment Type" class="form-control bg-light">
                              
                          <?php $banks =$investment->getAllBanks();
																foreach ($banks as $bank) {
																	echo  $bank['name'];
															 ?>
														<option value="<?php  echo $bank['id'] ?>" class="option"><?php echo  $bank['name'] ?></option>
														<?php 	} ?>
                             </select>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="BVN">Account Number</label>
                          <input type="number" value="<?php if (isset($_SESSION['bank_account_number'])) { echo $_SESSION['bank_account_number']; } ?>" name="bank_account_number"  maxlength="10" class="form-control bg-light"  placeholder="Account Number">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Account Name">Account Name</label>
                            <input type="text" class="form-control bg-light" value="<?php if (isset($_SESSION['bank_account_name'])) { echo $_SESSION['bank_account_name']; } ?>" name="bank_account_name" placeholder="Account Name">
                        </div>
                      </div>
                      <!-- hiiden field -->
                      <div class="form-row" >
												<label for="" hidden >
													Account services:
												</label>
												<div class="form-holder" hidden>
													<select name="account_service_id" class="form-control" hidden>
														<?php
																		$services = $investment->getAccountServices();
																		 foreach ($services as $service) {
														 ?>
															<option value="<?php  echo $service['id'] ?>" class="option"><?php echo  $service['title'] ?></option>
															<?php 	} ?>

														</select>
                            </div>
											</div>                    
                      <a href="step3.php" class="btn btn-outline-primary btn-lg" role="button" >Previous</a>
                      <button type="submit" name="applicants" class="btn btn-info  btn-lg float-right">Submit</button>
                    </form>
            </div>
        </div>
      </div>
<script>

</script>
      
    <div class="footer"> 

<p class="foottext py-4">2019 &#169;
All right reserved by Coralstone Capital  </p>

<img class="footlogo" style="position:absolute; top:20%; left:70%;" 
 src="https://referral.coralstone.capital/images/footlogo.svg">

</div>



</body>
</html>