
<?php
session_start();
if(!isset($_SESSION['surname'])){
      header("location: step1.php");
  }

include  ('vendor/autoload.php');
$investment = new Investment;
// surname, name, othernames, residential_address, phone_number, 	bvn,
// gender, date_of_birth, 	occupation, employer, employment_position, office_address, nationality,
// bank_account_number, bank_account_name, bank_id, next_of_kin_name, next_of_kin_address, next_of_kin_phone_number,
// next_of_kin_relationship, next_of_kin_email, account_service_id, investment_amount, tenor, email, investor_type_id, status, user_id

 if(isset($_POST['step2'])){
     
      // $_SESSION['investment_amount'] = $_POST['investment_amount'];
      // $_SESSION['tenor']= $_POST['tenor'];
      // $_SESSION['bvn'] = $_POST['bvn'];
      $_SESSION['occupation'] = $_POST['occupation'];
      $_SESSION['employer'] = $_POST['employer'];
      $_SESSION['employment_position'] = $_POST['employment_position'];
      $_SESSION['office_address'] = $_POST['office_address'];
      // $_SESSION['bank_id']= $_POST['bank_id'];
      // $_SESSION['bank_account_number'] = $_POST['bank_account_number'];
      // $_SESSION['bank_account_name'] = $_POST['bank_account_name'];

      if($_SESSION['occupation']  == ""){
          $applicants_error_message= 'Pleased eneter your occupation!';
      }elseif($_SESSION['employer'] == ""){
          $applicants_error_message= 'Pleased eneter your employer!';
      }elseif($_SESSION['employment_position'] == ""){
          $applicants_error_message= 'Pleased eneter your employment position!';
      }elseif($_SESSION['office_address'] == ""){
          $applicants_error_message= 'Pleased eneter your office address!';
      }else{

        header("Location: step3.php");
      }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Investment Application Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href=" https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <!-- Custorm sytle css -->
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

<nav class="navbar navbar-expand-sm bg-white shadow-sm">
  <!-- Brand/logo -->
  <a class="navbar-brand logo" href="#">
    <img src="https://referral.coralstone.capital/images/logo.png" alt="logo" style="width:100px;">
  </a>
</nav>

    <!-- <div class="containerse">
        <ul class="progressbarse">
            <li >Bio-Data</li>
            <li class="active">Employment Info</li>
            <li>Next of Kin</li>
            <li >Investment Details</li>
        </ul>
    </div>
    <br> -->
    <div class="container py-5">
      
        <div class="row col-12">
          <div class="col-12 col-md-5">
            <h5 class="">Investment Registration</h5>
              
          </div>
          
           <div class="col-12 col-md-7">
                <ul id="progressbar" class="text-center">
                    <li class="active step0">Bio-Data</li>
                    <li class="active step0">Employment Info</li>
                    <li class=" step0">Next of Kin</li>
                    <li class="step0">Investment Details</li>
                </ul>
            </div>
        </div>
      </div>
    
      <div class="container bg-white rounded">
      <?php
          if(isset($applicants_error_message) and $applicants_error_message != ""){
              echo '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                  <span class="badge badge-pill badge-danger">Danger  <br></span> '
                  .$applicants_error_message.'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>';
          }

            ?>
        <div class="row col-12 py-5">
        
            <div class="col-12 col-md-4 ">
            <h4 class="py-2">Employment Information</h4>
            <p>Tell us about where you work and your source of income</p>
              
          </div>
           <div class="col-12 col-md-8 py-5">

               <form action="" method="post">
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="Occupation">Occupation</label>
                          <input type="text" class="form-control bg-light" value="<?php if (isset($_SESSION['occupation'])) { echo $_SESSION['occupation']; } ?>" name="occupation"  placeholder="Occupation">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="Employer">Employer</label>
                          <input type="text" class="form-control bg-light" value="<?php if (isset($_SESSION['employer'])) { echo $_SESSION['employer']; } ?>" name="employer" placeholder="Employer">
                        </div>
                      </div>

                       <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="Employment Position">Employment Position</label>
                          <input type="text" class="form-control bg-light" value="<?php if (isset($_SESSION['employment_position'])) { echo $_SESSION['employment_position']; } ?>" name="employment_position"  placeholder="Employment Position">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="Office Address">Office Address</label>
                        <input type="text" class="form-control bg-light" value="<?php if (isset($_SESSION['office_address'])) { echo $_SESSION['office_address']; } ?>" name="office_address"  placeholder="Office Address">
                      </div>
                    
                      <a href="step1.php" class="btn btn-outline-primary btn-lg" role="button" >Previous</a>
                      <button type="submit" name="step2" class="btn btn-info  btn-lg float-right">Continue</button>
                    </form>
            </div>
        </div>
      </div>


    <div class="footer"> 

<p class="foottext py-4">2019 &#169;
All right reserved by Coralstone Capital  </p>

<img class="footlogo" style="position:absolute; top:20%; left:70%;" 
 src="https://referral.coralstone.capital/images/footlogo.svg">

</div>



</body>
</html>