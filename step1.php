
<?php
session_start();
include  ('vendor/autoload.php');
$investment = new Investment;
// surname, name, othernames, residential_address, phone_number, 	bvn,
// gender, date_of_birth, 	occupation, employer, employment_position, office_address, nationality,
// bank_account_number, bank_account_name, bank_id, next_of_kin_name, next_of_kin_address, next_of_kin_phone_number,
// next_of_kin_relationship, next_of_kin_email, account_service_id, investment_amount, tenor, email, investor_type_id, status, user_id

   if(isset($_POST['step1'])){

        $_SESSION['surname'] = $_POST['surname'];
        $_SESSION['name']= $_POST['name'];
		$_SESSION['othernames'] = $_POST['othernames'];
		$_SESSION['residential_address'] = $_POST['residential_address'];
		// $_SESSION['investor_type_id'] = $_POST['investor_type_id'];
		$_SESSION['nationality'] = $_POST['nationality'];
		$_SESSION['gender'] = $_POST['gender'];
    // $date_of_birth = date('Y-m-d', strtotime($_POST['date_of_birth']));
    // $date_of_birth = $_POST['date_of_birth'];
    // $show_date = DateTime::createFromFormat('d-m-Y', $date_of_birth)->format('Y-m-d');
    // $DateTime = new DateTime($_POST['date_of_birth']);
		$_SESSION['date_of_birth'] = $_POST['date_of_birth'];
		$_SESSION['email'] = $_POST['email'];
		$_SESSION['phone_number'] = $_POST['phone_number'];

				if($_SESSION['surname'] ==""){
            $applicants_error_message='Pleased enter your surname!';
        }elseif($_SESSION['name'] == ""){
            $applicants_error_message= 'Pleased eneter your name!';

        }elseif($_SESSION['othernames'] == ""){
            $applicants_error_message= 'Pleased eneter your othernames!';
        }elseif($_SESSION['residential_address']  == ""){
            $applicants_error_message= 'Pleased eneter your residential address!';
        }elseif($_SESSION['nationality'] == ""){
            $applicants_error_message= 'Pleased eneter your nationality!';
        }elseif($_SESSION['gender'] == ""){
            $applicants_error_message= 'Pleased eneter your gender!';
        }elseif($_SESSION['date_of_birth']== ""){
            $applicants_error_message= 'Pleased eneter your date of birth!';
        }elseif($_SESSION['email'] == ""){
            $applicants_error_message= 'Pleased eneter your email!';
        }elseif($_SESSION['phone_number'] == ""){
            $applicants_error_message= 'Pleased eneter your phone number!';
           // Check if Email Exist
        }elseif($investment->checkEmailExist($_SESSION['email'])){
            $applicants_error_message= 'Email already exist!.';
      }else{
                header("Location: step2.php");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Investment Application Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,700,800|Roboto:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="vendor/date-picker/css/datepicker.min.css">
  <!-- Custorm sytle css -->
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

<nav class="navbar navbar-expand-sm bg-white shadow-sm">
  <!-- Brand/logo -->
  <a class="navbar-brand logo" href="#">
    <img src="https://referral.coralstone.capital/images/logo.png" alt="logo" style="width:100px;">
  </a>
</nav>
    <div class="container py-5">
      
        <div class="row col-12">
          <div class="col-12 col-md-5">
            <h5 class="">Investment Registration</h5>
              
          </div>
          
           <div class="col-12 col-md-7">
                <ul id="progressbar" class="text-center">
                    <li class="active step0"></li>
                    <li class=" step0"></li>
                    <li class=" step0"></li>
                    <li class="step0"></li>
                </ul>
            </div>
        </div>
      </div>

      <div class="container bg-white rounded">
      <?php
          if(isset($applicants_error_message) and $applicants_error_message != ""){
              echo '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                  <span class="badge badge-pill badge-danger">Danger  <br></span> '
                  .$applicants_error_message.'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>';
          }

            ?>
        <div class="row col-12 py-5">
          <div class="col-12 col-md-4 ">
            <h4 class="py-2">Bio Data</h4>
            <p>Please fill in your personal details every field is required</p>
              
          </div>
           <div class="col-12 col-md-8 py-5">
               <form action="" method="post">
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="firstname">Surname Name</label>
                          <input type="text" name="surname" class="form-control bg-light" value="<?php if (isset($_SESSION['surname'])) { echo $_SESSION['surname']; } ?>" placeholder="Surname Name">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="othername">Other Name</label>
                          <input  type="text" name="name" class="form-control bg-light" value="<?php if (isset($_SESSION['name'])) { echo $_SESSION['name']; } ?>"  placeholder="Other Name">
                        </div>
                      </div>

                       <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="lastname">Last Name</label>
                          <input type="text" class="form-control bg-light" name="othernames" value="<?php if (isset($_SESSION['othernames'])) { echo $_SESSION['othernames']; } ?>" placeholder="Last Name">
                        </div>

                        <div class="form-group col-md-6">
                          <label for="gender">Gender</label>
                          <select name="gender" class="form-control bg-light">
                            <option selected>Choose...</option>
                            <option>Male</option>
                            <option>Female</option>
                          </select>
                        </div>
                      </div>
                      
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="lastname">Email</label>
                          <input type="text" class="form-control bg-light" value="<?php if (isset($_SESSION['email'])) { echo $_SESSION['email']; } ?>"  name="email" placeholder="Last Name">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="gender">Phone </label>
                         <input type="text" class="form-control bg-light" value="<?php if (isset($_SESSION['phone_number'])) { echo $_SESSION['phone_number']; } ?>" name="phone_number" placeholder="Last Name">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputAddress2">Address </label>
                        <input type="text" name="residential_address" class="form-control bg-light" value="<?php if (isset($_SESSION['residential_address'])) { echo $_SESSION['residential_address']; } ?>" placeholder="Residencial Address">
                      </div>

                      <div class="form-row">
                       
                        <div class="form-group col-md-6">
                          <label for="inputState">Nationality</label>
                          <select name="nationality" id="inputState" class="form-control bg-light">
                            <option value="<?php if (isset($_SESSION['nationality'])) { echo $_SESSION['nationality']; } ?>"  selected>Choose...</option>
                            <option >Nigeria</option>
                          </select>
                        </div>

                        <div class="form-group col-md-6">
                          <label for="inputState">Date of Birth</label>
                          <input type="date" value="<?php if (isset($_SESSION['date_of_birth'])) { echo $_SESSION['date_of_birth']; } ?>"  name="date_of_birth" class="form-control datepicker-here" data-language='en' data-date-format="yyyy-mm-dd" id="dp1">
                        </div>
                        </div>
                        <button type="submit" name="step1" class="btn btn-info  btn-lg float-right">Continue</button>
                    
                     
                    </form>
            </div>
        </div>
      </div>


    <div class="footer"> 

<p class="foottext py-4">2019 &#169;
All right reserved by Coralstone Capital  </p>

<img class="footlogo" style="position:absolute; top:20%; left:70%;" 
 src="https://referral.coralstone.capital/images/footlogo.svg">

</div>



</body>
</html>