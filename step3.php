
<?php
session_start();
if(!isset($_SESSION['occupation'])){
        header("location: step2.php");
    }
include  ('vendor/autoload.php');
$investment = new Investment;
// surname, name, othernames, residential_address, phone_number, 	bvn,
// gender, date_of_birth, 	occupation, employer, employment_position, office_address, nationality,
// bank_account_number, bank_account_name, bank_id, next_of_kin_name, next_of_kin_address, next_of_kin_phone_number,
// next_of_kin_relationship, next_of_kin_email, account_service_id, investment_amount, tenor, email, investor_type_id, status, user_id

   if(isset($_POST['step3'])){
        // session_start();
        $_SESSION['next_of_kin_name'] = $_POST['next_of_kin_name'];
        $_SESSION['next_of_kin_relationship'] = $_POST['next_of_kin_relationship'];
        $_SESSION['next_of_kin_email'] = $_POST['next_of_kin_email'];
        $_SESSION['next_of_kin_phone_number'] = $_POST['next_of_kin_phone_number'];
        $_SESSION['next_of_kin_address'] = $_POST['next_of_kin_address'];
        // $_SESSION['account_service_id'] = $_POST['account_service_id'];

		if($_SESSION['next_of_kin_name'] ==""){
            $applicants_error_message='Pleased enter next of kin name!';
        }elseif($_SESSION['next_of_kin_relationship'] == ""){
            $applicants_error_message= 'Pleased eneter next of kin relationship!';
        }elseif($_SESSION['next_of_kin_email'] == ""){
            $applicants_error_message= 'Pleased eneter next of kin email!';
        }elseif($_SESSION['next_of_kin_phone_number'] == ""){
            $applicants_error_message= 'Pleased eneter next of kin phone number!';
        }elseif($_SESSION['next_of_kin_address'] == ""){
            $applicants_error_message= 'Pleased eneter next of kin phone number!';
                    // Check if Email Exist
        }elseif($investment->checkEmailExist($_SESSION['email'])){
            $applicants_error_message= 'Email already exist! Pleased go back to step 1 to enter another Email or Re-enter this.';
            //Check if BVN exist
        }else{
        		
                header("Location: step4.php");
            }
        }
    
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Investment Application Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,700,800|Roboto:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <!-- Custorm sytle css -->
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

<nav class="navbar navbar-expand-sm bg-white shadow-sm">
  <!-- Brand/logo -->
  <a class="navbar-brand logo" href="#">
    <img src="https://referral.coralstone.capital/images/logo.png" alt="logo" style="width:100px;">
  </a>
</nav>

    <!-- <div class="containerse">
        <ul class="progressbarse">
            <li >Bio-Data</li>
            <li class="active">Employment Info</li>
            <li>Next of Kin</li>
            <li >Investment Details</li>
        </ul>
    </div>
    <br> -->
    <div class="container py-5">
      
        <div class="row col-12">
          <div class="col-12 col-md-5">
            <h5 class="">Investment Registration</h5>
              
          </div>
          
           <div class="col-12 col-md-7">
                <ul id="progressbar" class="text-center">
                    <li class="active step0">Bio-Data</li>
                    <li class="active step0">Employment Info</li>
                    <li class="active step0">Next of Kin</li>
                    <li class="step0">Investment Details</li>
                </ul>
            </div>
        </div>
      </div>

      <div class="container bg-white rounded">
      <?php
            if(isset($applicants_error_message) and $applicants_error_message != ""){
                echo '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <span class="badge badge-pill badge-danger">Danger  <br></span> '
                    .$applicants_error_message.'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>';
             }

            ?>
					 
        <div class="row col-12 py-5">
            <div class="col-12 col-md-4 ">
            <h4 class="py-2">Next of Kin</h5>
            <p>Who do you want us to transfer the ownership of your account to in the event of eventuality?</p>
              
          </div>
          
           <div class="col-12 col-md-8 py-5">

               <form action=""  method="post">
                      <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="full Name">Full Name</label>
                          <input type="text" value="<?php if (isset($_SESSION['next_of_kin_name'])) { echo $_SESSION['next_of_kin_name']; } ?>"  name="next_of_kin_name"class="form-control bg-light"  placeholder="Full Name">
                       </div>
                       <div class="form-group col-md-6">
                          <label for="Phone">Relationship</label>
                          <input type="text" value="<?php if (isset($_SESSION['next_of_kin_relationship'])) { echo $_SESSION['next_of_kin_relationship']; } ?>" name="next_of_kin_relationship" class="form-control bg-light"  placeholder="Next of Kin Relationship">
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="Email">Email</label>
                          <input type="email" value="<?php if (isset($_SESSION['next_of_kin_email'])) { echo $_SESSION['next_of_kin_email']; } ?>" name="next_of_kin_email" class="form-control bg-light"  placeholder="Next of Kin Email">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="Phone">Phone</label>
                          <input type="tel" value="<?php if (isset($_SESSION['next_of_kin_phone_number'])) { echo $_SESSION['next_of_kin_phone_number']; } ?>" name="next_of_kin_phone_number" class="form-control bg-light"  placeholder="Next of Kin Phone Number">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="Office Address">Office Address</label>
                        <input type="text" name="next_of_kin_address" value="<?php if (isset($_SESSION['next_of_kin_address'])) { echo $_SESSION['next_of_kin_address']; } ?>"  class="form-control bg-light"  placeholder="Office Address">
                      </div>

                       <a href="step2.php" class="btn btn-outline-primary btn-lg" role="button" >Previous</a>
                      <button type="submit" name="step3" class="btn btn-info  btn-lg float-right">Continue</button>
                    </form>
            </div>
        </div>
      </div>


    <div class="footer"> 

<p class="foottext py-4">2019 &#169;
All right reserved by Coralstone Capital  </p>

<img class="footlogo" style="position:absolute; top:20%; left:70%;" 
 src="https://referral.coralstone.capital/images/footlogo.svg">

</div>



</body>
</html>